# Step 2: Implement win logic

> Languages: JavaScript

Implement win logic via switch cases.
```
switch("a" + "|" + "b") {
    case "a|b":
        console.log("hello")
        break
    case "b|a":
        console.log("world")
        break
}
```

## Now it's your turn!
### Task 1:
Create an function which figures out, who wins.
This function have two parameters, one for the player hand and one for the enemy hand.
Example: `function whoWins(playerhand, enemyhand) {}`

### Task 2:
Return the result of the game to the console.
