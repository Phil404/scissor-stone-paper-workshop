// possible hands
const hands = ["Scissor", "Stone", "Paper"]

// pick a random hand for enemy
function giveEnemyHand() {
    let item = Math.floor(Math.random() * hands.length);
    return hands[item];
}

// set hand for player
function setHand(handName) {
    let enemyHand = giveEnemyHand()
    console.log(handName + " vs " + enemyHand)
    console.log(checkForWinner(handName, enemyHand))
}

function checkForWinner(playerHand, enemyHand) {
    switch (playerHand + ":" + enemyHand) {
        case hands[0] + ":" + hands[0]: // Both have scissor
        case hands[1] + ":" + hands[1]: // Both have stone
        case hands[2] + ":" + hands[2]: // Both have paper
            return "Draw!";
        case hands[0] + ":" + hands[2]: // Player have scissor & enemy have paper
        case hands[1] + ":" + hands[0]: // Player have stone & enemy have scissor
        case hands[2] + ":" + hands[1]: // Player have paper & enemy have stone
            return "Player wins!"
        case hands[0] + ":" + hands[1]: // Player have scissor & enemy have stone
        case hands[1] + ":" + hands[2]: // Player have stone & enemy have paper
        case hands[2] + ":" + hands[0]: // Player have paper & enemy have scissor
            return "Enemy wins!"
    }
}
