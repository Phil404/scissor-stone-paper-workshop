# Step 1: Initialize HTML File

> Languages: HTML, CSS and JavaScript

In this step we will learn how to set up our first HTML file including preparations for CSS and JavaScript. 

### Basic HTML Structure
The `<!DOCTYPE html>` tag is a relict of the long history. 
In the past there were multiple versions of HTML until HTML5.
Since there it's the current version and features come into the version without a version change.
The `<html>` tag is the most important tag for the basic structure, everything within this tag rendered by the browser.
In this structure there a two types of content. 
There are visible content in the `<body>` tag, there is the main part of the html content.
Another part is the `<head>` tag, there is everything related to the meta information of your page. 
```
<!DOCTYPE html>
<html lang="en">
<head>
    
</head>
<body>
    
</body>
</html>
```

### Meta Information in `<head>`
In the `<head>` area are all relevant information about your page stored.
The `<meta charset="UTF-8">` tag defines the character set of your page.
For example the `UTF-8` charset is required to type german characters like ä, ü or ß.
The `<title>` tag is self explained, it`s the title of your page which is displayed at the browser tab.

Also the CSS is stored in `<style>` tag. 
There you can define the style of the page.
You can write CSS styles in the html code (which is not recommended).
Otherwise you can outsource the CSS styles into another file.
This way is recommended and is done by `<link rel="stylesheet" href="style.css">`.
But you can name the css file as you want.
```
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!-- Anything related to the visuals by CSS. <style> -->
    <style>

    </style>
    <!-- Alternative for CSS: Link a css file. <link> -->
    <link rel="stylesheet" href="style.css">
</head>
```

### Body contains your real page
Body is self explained.
The `<script>` tag is the place for the javascript code. 
It is also not recommended to write the html file.
To outsource the javascript code simply add `src=scripts.js"` to the `<script>` tag.  
```
<body>
    <h1>Hello World!</h1>

    <!-- JavaScript Code Block. <script> -->
    <script>

    </script>
    <!-- Alternative for JavaScript: link a js file. <script src="scripts.js"></script> -->
    <script src="scripts.js">

    </script>
</body>
```

## Useful Links
- https://www.w3schools.com/html/default.asp