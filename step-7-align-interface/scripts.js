// possible hands
const hands = ["Scissor", "Stone", "Paper"]

// store for game score
let score_player = 0
let score_enemy = 0

// pick a random hand for enemy
function giveEnemyHand() {
    let item = Math.floor(Math.random() * hands.length)
    return hands[item]
}

// set hand for player
function setHand(handName) {
    let enemyHand = giveEnemyHand()
    console.log(handName + " vs " + enemyHand)
    updateWinner(checkForWinner(handName, enemyHand)) // print game result
    console.log("Score: Player [" + score_player + "] - Enemy [" + score_enemy + "]") // print score
}

function checkForWinner(playerHand, enemyHand) {
    switch (playerHand + ":" + enemyHand) {
        case hands[0] + ":" + hands[0]: // Both have scissor
        case hands[1] + ":" + hands[1]: // Both have stone
        case hands[2] + ":" + hands[2]: // Both have paper
            return "Draw!"
        case hands[0] + ":" + hands[2]: // Player have scissor & enemy have paper
        case hands[1] + ":" + hands[0]: // Player have stone & enemy have scissor
        case hands[2] + ":" + hands[1]: // Player have paper & enemy have stone
            score_player++ // add score for player
            return "Player wins!"
        case hands[0] + ":" + hands[1]: // Player have scissor & enemy have stone
        case hands[1] + ":" + hands[2]: // Player have stone & enemy have paper
        case hands[2] + ":" + hands[0]: // Player have paper & enemy have scissor
            score_enemy++; // add score for enemy
            return "Enemy wins!"
    }
}

function resetScore() {
    score_player = 0
    score_enemy = 0
    updateScore()
}

function updateWinner(winner) {
    document.getElementById("win").innerText = winner
    updateScore()
}

function updateScore() {
    document.getElementById("player_score").innerText = score_player
    document.getElementById("enemy_score").innerText = score_enemy
}

