# Step 4: Implement score logic

> Languages: JavaScript

In this step we will learn how JavaScript functions work and how to use them.

## Now it's your turn!
### Task 1:
Create two variables for the score of the enemy and the player.
Both variables start with `0`.

### Task 2:
When player wins, increase the score for the player.
When enemy wins, increase the score for the enemy.

### Task 3:
Create a function to reset the score.
