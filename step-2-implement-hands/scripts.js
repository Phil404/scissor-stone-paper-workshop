// possible hands
const hands = ["Scissor", "Stone", "Paper"]

// pick a random hand for enemy
function giveEnemyHand() {
    let item = Math.floor(Math.random() * hands.length);
    return hands[item];
}

// set hand for player
function setHand(handName) {
    console.log(handName + " vs " + giveEnemyHand())
}