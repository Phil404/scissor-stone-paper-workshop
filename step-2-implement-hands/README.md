# Step 2: Implement functions for the hands

> Languages: JavaScript

In this step we will learn how JavaScript functions work and how to use them.

### Console output
For fast development and scripting it is cool to print out results or information into the console:
```
console.log("Print my information to the console")
```

### Comments
To add comments in JavaScript you can use the following ways: 
```
// I am a single line comment

var name = "value" // I can also stand behind code

/*
    I am a multiline comment!
*/
```

### Variables
For storing information variables are a very useful thing to use.
To use them it is quiet simple:
```
var myVariableName = "My Information"
```
This is the classic form of a variable.
But there are specific types of variables.
```
let name = "I am a variable which can change their information"

const name = "I am a variable which defines their information once"

const array = ["I", "have", "multiple", "information"]

// Usage of an variable:
console.log(name)
```

### Functions
Functions are essential for code. 
These are used for repeatable task and to simplify your code.
They execute code and can give with `return "hello world"` information back to the call.
```
function myFunctionName() {
    console.log("Hello world!")
}

function myFunctionName(parameter) {
    console.log(parameter)
    return parameter
}
```

## Now it's your turn!
### Task 1:
Define an array which contains three values.
These values are the three possible hands in the game.

### Task 2:
Define a function to select a random value from this array.
The function should return the picked value.
Useful codesnippet: 
```
// This snippet picks a random item from an array. Change 'array' to the name of your array from Task 1.
Math.floor(Math.random() * array.length)
```

### Task 3:
Define a function where you can place a parameter via the call.
This function should print both hands to the console.
The parameter should contain the hand of the player.
The function from task 2 represents an enemy.

